 // output units are mm when exporting as DXF
$fa = 0.01;
$fs = 2;
MIL_IN_MM = 39.370078740157;

board_thickness = 63; // 1.6002mm
board_thickness_mm = board_thickness / MIL_IN_MM;
bt_mm = board_thickness_mm;

margin = 10; // 0.254mm
margin_mm = margin / MIL_IN_MM;

// bt_mm + margin = 1.8542mm

tab = bt_mm * 3;
tab_inset = tab + margin_mm;

dia = 15.5;

// slot_width is the cutout length to clear the 63mil thick board
slot_width = bt_mm + margin_mm; // 1.6002mm + 0.254mm= 1.8542mm

pwrbrd_clearance = 3.146;

render_power = true;
render_lower = true;
render_data = true;
render_upper = true;

lipo_width = 19;
lipo_length = 21;

pwrbrd_x = 0;
pwrbrd_y = 40;
pwrbrd_width = 23;
pwrbrd_length = 20; // UNUSED

tab_width = 4; // just a guess
mid_width = 10; // just a guess
mid_length = 15; // just a guess

upper_x = 0;
upper_y = 125;

bb_x = 0;
bb_y = 0;

dp_x = 0;
dp_y = 85;
dp_width = mid_width;
dp_length = lipo_length + mid_length;

// upper board
if(render_upper) {
  translate([ upper_x, upper_y ]) {
    difference() {
      circle(dia);
  
      translate([ -(tab_inset/2), 1.15 + slot_width ])
        square([ tab_inset, slot_width ]);
    
      dp_slot();
    }
  }
}

// lower board
if(render_lower) {
  translate([ bb_x, bb_y ])
  difference() {
    circle(dia);
    
    power_board_slot(0, 4);
  
    dp_slot();
  }
}

// data pillar
if(render_data) {
  translate([ 0, -(dp_length / 2) ]) {
    translate([ dp_x, dp_y ]) {
      translate( [ -(tab / 2), -bt_mm ])
        square([ tab, bt_mm ]);
      
      translate([ -(dp_width / 2), 0 ])
        square([ dp_width, dp_length ]);
      
      translate( [ -(tab / 2), dp_length ])
        square([ tab, bt_mm ]);
    }
  }
}

// power board
// contains: LiPo, 3v3 reg (back/inside)
// layout:
// upper tab, joins power board and led board
// mid section, can be used to adjust overall length of power board
// lipo holder, holder LiPo charger and it's lower shoulders are the base board joint
// lower holder, extends past the base board exposing the USB port
if(render_power) {
  translate([ pwrbrd_x, pwrbrd_y - (lipo_length + mid_length - tab) / 2 ]) {
  // mid section
    translate([  -(mid_width / 2), lipo_length ]) {
      square([ mid_width, mid_length ]);
      
      // upper tab, connects to LED board
      translate([ (mid_width / 2) - (tab / 2), mid_length ])
        square([ tab, bt_mm ]);
    }
    
    // width enough to fit the slot,
    // but it only needs to be as 'tall' as the lipo board before narrowing    
    translate([  -(pwrbrd_width / 2), 0 ]) {
      square([ pwrbrd_width, lipo_length ]);
    }    
    
    // this portion of the power board extends past the base board
    // pushing the usb port out
    translate([ -(lipo_width) / 2, -pwrbrd_clearance ])
      square([ lipo_width, pwrbrd_clearance ]);
  }
}

module dp_slot() {
  translate([ -(tab_inset/2), -(11.2+slot_width) ])
    square([ tab_inset, slot_width ]);
}

module power_board_slot(x, y) { 
  slot(x, y, pwrbrd_width + margin_mm); // board mounting slot
  
  translate([ -(9.5), y + (slot_width) / 2 ])
    square([ lipo_width, pwrbrd_clearance ]);

}

module slot(x, y, length) {
  translate([ x - (length / 2), y - (slot_width / 2) ])
    square([ length, slot_width ]);
}